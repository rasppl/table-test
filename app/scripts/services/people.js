'use strict';

/**
 * @ngdoc service
 * @name tableTestApp.People
 * @description
 * # People
 * Service in the tableTestApp.
 */
angular.module('tableTestApp')
    .factory('People', function () {
        var people = [
            {
                name: 'Bartosz Dziamski',
                birth: '1984-10-14T23:00:00.000Z',
                email: 'bartek.dziamski@gmail.com',
                children: 1
            },
            {
                name: 'Joanna Krupa',
                birth: '1979-04-22T22:00:00.000Z',
                email: 'joanna@krupa.com',
                children: 2
            }
        ];

        return {
            query: function() {
                return people;
            },
            addPerson: function(newPerson) {
                people.push(newPerson);
            }
        };
    });
