## Get dependencies ##

1.
```
npm install
```

2.
```
bower install
```

## Run simple tests ##

```
grunt test
```

## Launch in browser ##

```
grunt serve
```