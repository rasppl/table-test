'use strict';

/**
 * @ngdoc overview
 * @name tableTestApp
 * @description
 * # tableTestApp
 *
 * Main module of the application.
 */
angular
    .module('tableTestApp', [
        'nccSortable'
    ])

    .config(function(nccSortableProvider) {
        nccSortableProvider.setTemplateUrl('views/templates/table.html');

    })

;
