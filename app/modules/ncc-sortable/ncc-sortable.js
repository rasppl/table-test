angular.module('nccSortable', [])

    .provider('nccSortable', function () {

        // Private variables
        var templateUrl = '';

        // Public API for configuration
        this.setTemplateUrl = function (arg) {
            templateUrl = arg;
        };

        // Method for instantiating
        this.$get = function () {
            return {
                getTemplateUrl: function() {
                    return templateUrl;
                }
            };
        }
    })

    .directive('nccTable', function(nccSortable) {
        return {
            templateUrl: nccSortable.getTemplateUrl(),
            scope: {
                'thead': '=',
                'tbody': '=',
                'order' : '=',
                'searchables' : '='
            },
            restrict: 'E',
            link: function(scope, element, attrs) {
                scope.predicate = scope.order.by;
                scope.reverse = scope.order.reverse;
                scope.sort = function(by) {
                    scope.predicate = by;
                    scope.reverse = !(scope.reverse);
                };
                scope.query = '',
                scope.search = function(item) {
                    var answer = false;
                    scope.searchables.forEach(function(element) {
                        if (item[element].indexOf(scope.query) != -1) {
                            answer = true;
                        }
                    });
                    return answer;
                }
            }
        };
    })
