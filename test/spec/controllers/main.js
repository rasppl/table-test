'use strict';

describe('Controller: MainCtrl', function () {

    // load the controller's module
    beforeEach(module('tableTestApp'));

    var MainCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        MainCtrl = $controller('MainCtrl', {
            $scope: scope
        });
    }));

    it('should define header', function () {
        expect(scope.header).not.toBe(undefined);
    });
    it('should define starting order', function () {
        expect(scope.startingOrder).not.toBe(undefined);
    });
    it('should attach a list of people to the scope', function () {
        expect(scope.people.length).toBe(2);
    });
});

describe('Controller: AddItemCtrl', function () {

    // load the controller's module
    beforeEach(module('tableTestApp'));

    var AddItemCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope, People) {
        scope = $rootScope.$new();
        scope.people = People.query();
        AddItemCtrl = $controller('AddItemCtrl', {
            $scope: scope
        });
    }));

    it('should add person to people', function () {
        var baseTime = new Date(1990, 9, 23);
        scope.person.name = 'John Doe';
        scope.person.birth = baseTime;
        scope.person.email = 'some@email.com';
        scope.person.children = 5;
        scope.addItem(scope.person);

        expect(scope.people.length).toBe(3);
    });

});
