'use strict';

angular.module('tableTestApp')

/**
 * @ngdoc function
 * @name tableTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tableTestApp
 */

    .controller('MainCtrl', ['$scope', 'People', function ($scope, People) {
        $scope.header = {
            name: 'Name',
            birth: 'Date of birth',
            email: 'Email address',
            children: 'Number of children'
        };
        $scope.people = People.query();
        $scope.startingOrder = {
            by: '+name',
            reverse: false
        };
    }])

    .controller('AddItemCtrl', ['$scope', 'People', function ($scope, People) {

        $scope.person = {};

        $scope.addItem = function(data) {
            People.addPerson(data);
            $scope.person = {};
        };

    }])
;
